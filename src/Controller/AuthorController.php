<?php

namespace App\Controller;

use App\Entity\Author;
use App\Repository\AuthorRepository;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Context\Normalizer\ObjectNormalizerContextBuilder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class AuthorController extends AbstractController
{
    #[Route('/api/authors', name: 'get_authors', methods: ['GET'])]
    public function getAllAuthors(
        AuthorRepository $auhtorRepository,
        SerializerInterface $serializer,
        Request $request,
        TagAwareCacheInterface $cache
    ): JsonResponse {

        $page = $request->get('page', 1);
        $limit = $request->get('limit', 3);

        $idCache = "GetAuthors-" . $page . "-" . $limit;
        $jsonAuthors = $cache->get($idCache, function (ItemInterface $item) use ($auhtorRepository, $page, $limit, $serializer) {
            echo ("L'objet n'est pas encore en cache ! ");
            $item->tag('authorsCache');
            $authors = $auhtorRepository->findAllWithPagination($page, $limit);

            // Choix du groupe a serialiser (cf. #[Groupes(['getAuthors])]) dans les entités
            $context = (new ObjectNormalizerContextBuilder())
                ->withGroups('getAuthors')
                ->toArray();

            // Convertir la liste d'objet Authors en Json
            return $serializer->serialize($authors, 'json', $context);
        });

        return new JsonResponse($jsonAuthors, Response::HTTP_OK, [], true);
    }

    #[Route('/api/authors/{id}', name: 'get_author',  methods: ['GET'])]
    public function getAuthor(SerializerInterface $serializer, Author $author): JsonResponse
    {
        // Choix du groupe a serialiser (cf. #[Groupes(['getAuthors])]) dans les entités
        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('getAuthors')
            ->toArray();

        //Convertir l'objet Author en Json
        $jsonAuthor = $serializer->serialize($author, 'json', $context);

        return new JsonResponse($jsonAuthor, Response::HTTP_OK, [], true);
    }

    #[IsGranted('ROLE_ADMIN', message: "Vous n'avez pas les droits sufisants pour cette action.")]
    #[Route('/api/authors/{id}', name: 'delete_author', methods: ['DELETE'])]
    public function deleteAuthor(EntityManagerInterface $em, Author $author, TagAwareCacheInterface $cache): JsonResponse
    {
        $cache->invalidateTags(['authorsCache']);
        $em->remove($author);
        $em->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[IsGranted('ROLE_ADMIN', message: "Vous n'avez pas les droits sufisants pour cette action.")]
    #[Route('/api/authors', name: 'add_author', methods: ['POST'])]
    public function addAuthor(
        SerializerInterface $serializer,
        Request $request,
        EntityManagerInterface $em,
        UrlGeneratorInterface $urlGenerator,
        BookRepository $bookRepository,
        ValidatorInterface $validator,
        TagAwareCacheInterface $cache
    ): JsonResponse {
        $author = $serializer->deserialize($request->getContent(), Author::class, 'json');

        $content = $request->toArray();
        if (isset($content['bookId'])) {
            $author->addBook($bookRepository->find($content['bookId']));
        }

        // On vérifie s'il y a des erreurs
        $error = $validator->validate($author);
        if ($error->count() > 0) {
            return new JsonResponse($serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);
            // throw new HttpException(JsonResponse::HTTP_BAD_REQUEST, $error->__toString());
        }

        $cache->invalidateTags(['authorsCache']);
        $em->persist($author);
        $em->flush();

        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('getAuthors')
            ->toArray();

        $jsonAuthor = $serializer->serialize($author, 'json', $context);

        $location = $urlGenerator->generate('get_author', ['id' => $author->getId()], UrlGeneratorInterface::ABSOLUTE_URL);

        return new JsonResponse($jsonAuthor, Response::HTTP_CREATED, ["location" => $location], true);
    }

    #[IsGranted('ROLE_ADMIN', message: "Vous n'avez pas les droits sufisants pour cette action.")]
    #[Route('/api/authors/{id}', name: 'update_author', methods: ['PUT'])]
    public function updateAuthor(
        SerializerInterface $serializer,
        Request $request,
        EntityManagerInterface $em,
        UrlGeneratorInterface $urlGenerator,
        BookRepository $bookRepository,
        Author $currentAuthor,
        ValidatorInterface $validator,
        TagAwareCacheInterface $cache
    ): JsonResponse {

        $updatedAuthor = $serializer->deserialize($request->getContent(), Author::class, 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $currentAuthor]);

        $content = $request->toArray();
        if (isset($content['bookId'])) {
            $updatedAuthor->addBook($bookRepository->find($content['bookId']));
        }

        // On vérifie s'il y a des erreurs
        $error = $validator->validate($updatedAuthor);
        if ($error->count() > 0) {
            return new JsonResponse($serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);
            // throw new HttpException(JsonResponse::HTTP_BAD_REQUEST, $error->__toString());
        }

        $cache->invalidateTags(['authorsCache']);
        $em->persist($updatedAuthor);
        $em->flush();

        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('getAuthors')
            ->toArray();

        $jsonAuthor = $serializer->serialize($updatedAuthor, 'json', $context);

        $location = $urlGenerator->generate('get_author', ['id' => $updatedAuthor->getId()], UrlGeneratorInterface::ABSOLUTE_URL);

        return new JsonResponse($jsonAuthor, Response::HTTP_PARTIAL_CONTENT, ["location" => $location], true);
    }
}
