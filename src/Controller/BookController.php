<?php

namespace App\Controller;

use App\Entity\Book;
use App\Repository\AuthorRepository;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Context\Normalizer\ObjectNormalizerContextBuilder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class BookController extends AbstractController
{
    #[Route('/api/books', name: 'get_books', methods: ['GET'])]
    public function getAllBooks(
        BookRepository $bookRepository,
        SerializerInterface $serializer,
        Request $request,
        TagAwareCacheInterface $cache
    ): JsonResponse {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 3);

        $idCache = "getAllBooks-" . $page . "-" . $limit;

        $jsonBooks = $cache->get($idCache, function (ItemInterface $item) use ($bookRepository, $page, $limit, $serializer) {
            echo ("L\'objet n'est pas encore en cache !");
            $item->tag("booksCache");
            $books = $bookRepository->findAllWithPagination($page, $limit);

            // Choix du groupe a serialiser (cf. #[Groupes(['getBooks])]) dans les entités
            $context = (new ObjectNormalizerContextBuilder())
                ->withGroups('getBooks')
                ->toArray();

            //Convertir la liste d'objet book en Json
            return $serializer->serialize($books, 'json', $context);
        });


        return new JsonResponse($jsonBooks, Response::HTTP_OK, [], true);
    }

    #[Route('/api/books/{id}', name: 'get_book', methods: ['GET'])]
    public function getBook(SerializerInterface $serializer, Book $book): JsonResponse
    {
        // Choix du groupe a serialiser (cf. #[Groupes(['getBooks])]) dans les entités
        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('getBook')
            ->toArray();

        //Convertir l'objet book en Json
        $jsonBook = $serializer->serialize($book, 'json', $context);

        return new JsonResponse($jsonBook, Response::HTTP_OK, [], true);
    }

    #[IsGranted('ROLE_ADMIN', message: "Vous n'avez pas les droits sufisants pour cette action.")]
    #[Route('/api/books/{id}', name: 'delete_book', methods: ['DELETE'])]
    public function deleteBook(EntityManagerInterface $em, Book $book, TagAwareCacheInterface $cache): JsonResponse
    {
        $cache->invalidateTags(['booksCache']);
        $em->remove($book);
        $em->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[IsGranted('ROLE_ADMIN', message: "Vous n'avez pas les droits sufisants pour cette action.")]
    #[Route('/api/books', name: 'add_book', methods: ['POST'])]
    public function addBook(
        SerializerInterface $serializer,
        Request $request,
        EntityManagerInterface $em,
        UrlGeneratorInterface $urlGenerator,
        AuthorRepository $authorRepository,
        ValidatorInterface $validator,
        TagAwareCacheInterface $cache
    ): JsonResponse {
        $book = $serializer->deserialize($request->getContent(), Book::class, 'json');

        $content = $request->toArray();
        if (isset($content['authorId'])) {
            $book->setAuthor($authorRepository->find($content['authorId']));
        }

        // On vérifie s'il y a des erreurs
        $error = $validator->validate($book);
        if ($error->count() > 0) {
            return new JsonResponse($serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);
            // throw new HttpException(JsonResponse::HTTP_BAD_REQUEST, $error->__toString());
        }

        $cache->invalidateTags(['booksCache']);
        $em->persist($book);
        $em->flush();

        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('getBook')
            ->toArray();

        $jsonBook = $serializer->serialize($book, 'json', $context);

        $location = $urlGenerator->generate('get_book', ['id' => $book->getId()], UrlGeneratorInterface::ABSOLUTE_URL);

        return new JsonResponse($jsonBook, Response::HTTP_CREATED, ["location" => $location], true);
    }

    #[IsGranted('ROLE_ADMIN', message: "Vous n'avez pas les droits sufisants pour cette action.")]
    #[Route('/api/books/{id}', name: 'update_book', methods: ['PUT'])]
    public function updateBook(
        SerializerInterface $serializer,
        Request $request,
        EntityManagerInterface $em,
        UrlGeneratorInterface $urlGenerator,
        AuthorRepository $authorRepository,
        Book $currentBook,
        ValidatorInterface $validator,
        TagAwareCacheInterface $cache
    ): JsonResponse {

        $updatedBook = $serializer->deserialize($request->getContent(), Book::class, 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $currentBook]);

        $content = $request->toArray();
        if (isset($content['authorId'])) {
            $updatedBook->setAuthor($authorRepository->find($content['authorId']));
        }

        // On vérifie s'il y a des erreurs
        $error = $validator->validate($updatedBook);
        if ($error->count() > 0) {
            return new JsonResponse($serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);
            // throw new HttpException(JsonResponse::HTTP_BAD_REQUEST, $error->__toString());
        }

        $cache->invalidateTags(['booksCache']);
        $em->persist($updatedBook);
        $em->flush();

        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('getBook')
            ->toArray();

        $jsonBook = $serializer->serialize($updatedBook, 'json', $context);

        $location = $urlGenerator->generate('get_book', ['id' => $updatedBook->getId()], UrlGeneratorInterface::ABSOLUTE_URL);

        return new JsonResponse($jsonBook, Response::HTTP_PARTIAL_CONTENT, ["location" => $location], true);
    }
}
